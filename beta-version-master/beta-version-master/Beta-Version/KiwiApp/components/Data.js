import React, { useState } from "react";
import { Intro, Sliders, Pomade, Laptop } from "../constants/Image";

const Introdata = [
  {
    key: "k1",
    title: "Quick & Easy \nCampus Shopping",
    text: "Get the hottest fashion by trend\n and season right on your pocket.",
    image: Intro.intro1,
  },
  {
    key: "k2",
    title: "Secure Payment",
    text: "All your payment information is top safety\n and protected",
    image: Intro.intro2,
  },
  {
    key: "k3",
    title: "Fast delivery",
    text: "Buy and sell in your comfort Zone",
    image: Intro.intro3,
  },
  {
    key: "k4",
    title: "High Performance",
    text: " Saving your value time and buy product with ease",
    image: Intro.intro4,
  },
  {
    key: "k5",
    title: "Restaurant Bookings",
    text: " 20% off on first Restaurant booking",
    image: Intro.intro5,
  },
];

const LaptopsData = [
  {
    title: "Hp Envy",
    image: Laptop.Laptop1,
    description: "Core-i7,16GB RAM,1 TB HDD",
    id: 1,
  },
  {
    title: "MacBookPro",
    image: Laptop.Laptop2,
    description: "Core-i7,16GB RAM,2 TB HDD,3.0 GHz",
    id: 2,
  },
  {
    title: "Macbook Air 2019",
    image: Laptop.Laptop3,
    description: "Core-i7,16GB RAM,1 TB HDD",
    id: 3,
  },
  {
    title: "Dell Latitude E875",
    image: Laptop.Laptop4,
    description: "Core-i9,12GB RAM,2 TB HDD,3.0 GHz",
    id: 4,
  },
  {
    title: "Hp Spectra 2019",
    image: Laptop.Laptop5,
    description: "Core-i9,16GB RAM,2 TB HDD,3.1 GHz,256 SSD",
    id: 5,
  },
];
const PomadeData = [
  {
    key: "1",
    title: "Nike Easy Boost",
    desc: "Get the hottest fashion by trend and season right\n on your pocket.",
    image: Pomade.Pomade1,
  },
  {
    key: "2",
    title: "Secure Payment",
    desc: "All your payment information is top safety\n and protected",
    image: Pomade.Pomade2,
  },
  {
    key: "3",
    title: "Fast delivery",
    desc: "Buy and sell in your comfort Zone",
    image: Pomade.Pomade3,
  },
  {
    key: "4",
    title: "Casio fx",
    desc: " Saving your value time and buy product with ease",
    image: Pomade.Pomade4,
  },
];

export { LaptopsData, PomadeData, Introdata, Sliders };
