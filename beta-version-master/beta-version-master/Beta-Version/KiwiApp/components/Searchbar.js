import React, { Component } from "react";
import { View } from "react-native";
import { Icon, Item, Input } from "native-base";
class Searchbar extends Component {
  render() {
    return (
      <View>
        <View
          style={{
            position: "absolute",
            left: 0,
            right: 0,
            height: 70,
            backgroundColor: "#07485B",
            flexDirection: "row",
            alignItems: "center",
            paddingHorizontal: 5,
          }}
        >
          <View
            style={{
              flex: 1,
              height: "100%",
              justifyContent: "center",
            }}
          >
            <Item
              style={{
                backgroundColor: "white",
                paddingHorizontal: 10,
                borderRadius: 4,
              }}
            >
              <Icon name="search" style={{ fontSize: 20, paddingTop: 5 }} />
              <Input placeholder="Search" />
            </Item>
          </View>
        </View>
      </View>
    );
  }
}

export default Searchbar;
