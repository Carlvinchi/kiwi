import React, { Component } from "react";
import { View, Image } from "react-native";
import Swiper from "react-native-swiper";

class ContentSwipper extends Component {
  render() {
    return (
      <View>
        <Swiper autoplay={true} style={{ height: 100 }}>
          <View style={{ flex: 1 }}>
            <Image
              style={{
                flex: 1,
                height: null,
                width: null,
                resizeMode: "stretch"
              }}
              source={require("../assets/Images/Slider1.png")}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Image
              style={{
                flex: 1,
                height: null,
                width: null,
                resizeMode: "stretch"
              }}
              source={require("../assets/Images/Slider2.png")}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Image
              style={{
                flex: 1,
                height: null,
                width: null,
                resizeMode: "stretch"
              }}
              source={require("../assets/Images/Slider2.png")}
            />
          </View>
          <View style={{ flex: 1 }}>
            <Image
              style={{
                flex: 1,
                height: null,
                width: null,
                resizeMode: "cover"
              }}
              source={require("../assets/Images/Slider2.png")}
            />
          </View>
        </Swiper>
      </View>
    );
  }
}
export default ContentSwipper;
