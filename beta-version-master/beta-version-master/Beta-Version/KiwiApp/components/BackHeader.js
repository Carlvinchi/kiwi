import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default class BackHeader extends Component {
  render() {
    return (
      <View style={{ flexDirection: "row" }}>
        <Ionicons style={styles.backIcon} name="ios-arrow-back" size={20} />
        <Text style={styles.backTxt}>Back</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backTxt: {
    fontSize: 20,
    color: "white",
    fontWeight: "bold",
    paddingLeft: 5,
    paddingVertical: 40,
    paddingBottom:50
  },
  backIcon: {
    paddingTop: 45,
    color: "white",
  },
});
