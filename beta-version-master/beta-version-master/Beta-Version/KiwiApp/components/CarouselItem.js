import React from "react";
import { View, StyleSheet, Text, Image, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

const CarouselItem = ({ item }) => {
  return (
    <View style={styles.cardView}>
      <Image style={styles.image} source={item.image} />
      <View style={styles.textView}>
        <Text style={styles.itemTitle}>{item.title}</Text>
        <Text style={styles.itemDirection}>{item.description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    width: width - 20,
    height: height / 3,
    backgroundColor: "white",
    margin: 10,
    borderRadius: 15,
    shadowColor: "black",
    elevation: 5,
  },
  textView: {
    position: "absolute",
    bottom: 10,
    margin: 10,
    left: 5,
  },
  image: {
    width: width - 20,
    height: height / 3,
    borderRadius:15,
  },
  itemTitle: {
    color: "white",
    fontSize: 22,
    shadowColor: "#000",
    elevation: 5,
    marginBottom: 5,
    fontWeight: "bold",
  },
  itemDirection: {
    color: "white",
    fontSize: 12,
    shadowColor: "black",
    elevation: 5,
  },
});

export default CarouselItem;
