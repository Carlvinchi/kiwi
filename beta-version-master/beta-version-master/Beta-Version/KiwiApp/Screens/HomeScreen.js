import React, { Component } from "react";
import { TouchableOpacity, ScrollView, Image, StatusBar } from "react-native";
import Searchbar from "../components/Searchbar";
import { StyleSheet } from "react-native";
import { Container, Content, Card, View, Text } from "native-base";
import ContentSwiper from "../components/ContentSwiper";
import ItemsCard from "../components/ItemsCard";
import PomadeCard from "../components/PomadeCard";
import LaptopsCard from "../components/LaptopsCard";
import { LaptopsData, PomadeData } from "../components/Data";

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Products: [],
    };
  }

  fetchProduct = async () => {
    fetch("https://warm-ridge-56934.herokuapp.com/getProduct")
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        this.setState({ Products: data });
      });
  };

  UNSAFE_componentWillMount() {
    this.fetchProduct();
  }
  renderProducts() {
    return this.state.Products.map((Product) => (
      <Card key={Product._id}>
        <View style={styles.containerStyle}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("Details", {
                productId: Product._id,
                productPrice: Product.price,
                productImage: Product.images,
                productTitle: Product.title,
                productDescription: Product.description,
                WhatsApp: Product.whatsapp,
                Contact: Product.contact
              })
            }
          >
            <View style={styles.containerStyle_2}>
              <Image style={styles.image} source={{ uri: Product.images }} />
              <View style={styles.container_1}>
                <Text style={styles.textStyle}>{Product.title}</Text>
                <Text style={styles.textStyle}>{Product.price}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </Card>
    ));
  }
  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#07485B" barStyle="light-content" />
        <Searchbar />
        <Content style={{ marginTop: 70, backgroundColor: "#d5d5d5" }}>
          <ContentSwiper />
          <Card>
            <Text style={styles.Title}>Latest Deals</Text>
            <ItemsCard />
          </Card>
          <Card>
            <Text style={styles.Title}>Pomade</Text>
            <PomadeCard data={PomadeData} />
          </Card>
          <Card>
            <Text style={styles.Title}>Laptops</Text>
            <LaptopsCard data={LaptopsData} />
          </Card>

          {this.renderProducts()}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  Title: { fontWeight: "bold", margin: 10 },

  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    height: 280,

    width: 380,
  },
  container_1: {
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  containerStyle: {
    elevation: 0,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
  },
  containerStyle_2: {
    borderBottomWidth: 0,
    padding: 5,
    backgroundColor: "#fff",
    justifyContent: "flex-start",
    flexDirection: "column",
    borderColor: "#ddd",
    position: "relative",
  },
  textStyle: {
    fontSize: 18,
  },
});
