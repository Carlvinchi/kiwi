import React, { Component } from "react";
import { Text, View, ScrollView, Image } from "react-native";
import BackHeader from "../components/BackHeader";
import { Icons } from "../constants/Image";
import DefaultStyle from "../constants/Styles/DefaultStyles";
import { Card } from "native-base";
import { Avatar } from "react-native-paper";
import { IMAGE } from "../constants/Image";

const { TermsTitle } = DefaultStyle;

const StyledInfo = ({ name, number, email, icon }) => {
  return (
    <Card style={{ flexDirection: "row" }}>
      <Avatar.Image source={icon} size={70} />
      <View>
        <Text>{name}</Text>
        <Text>{number}</Text>
        <Text>{email}</Text>
      </View>
    </Card>
  );
};

const Styledheader = ({ title }) => {
  return (
    <View style={{ alignItems: "center" }}>
      <Text style={TermsTitle}>{title}</Text>
    </View>
  );
};

const ContactScreen = () => {
  return (
    <View style={{ backgroundColor: "white" }}>
      <View style={{ alignItems: "center" }}>
        <Text style={{ fontSize: 20, marginTop: 30, color: "black" }}>
          Contact
        </Text>
      </View>
      <ScrollView
        style={{
          margin: 20,
          backgroundColor: "#FFF",
          borderRadius: 20,
          marginBottom: 100,
        }}
      >
        <Text>Below are the Brains behind KIWI_Mart</Text>
        <Styledheader title="FrontEnd" />
        <StyledInfo
          icon={IMAGE.avatar}
          name="FELIX OPPONG KWADWO"
          number="048648"
          email="******@gmail.com"
        />
        <StyledInfo
          icon={IMAGE.avatar}
          name="ASAMOAH ANDREWS"
          number="0557462789"
          email="kasamoah715@gmail.com"
        />
        <Styledheader title="BackEnd" />
        <StyledInfo
          icon={IMAGE.avatar}
          name="Antwi Confidence"
          number="048648"
          email="******@gmail.com"
        />
        <StyledInfo
          icon={IMAGE.avatar}
          name="AKANDI MICHAEL"
          number="******"
          email="******@gmail.com"
        />
        <StyledInfo
          icon={IMAGE.avatar}
          name="ABDUL BASHIRU"
          number="0540633840"
          email="abdulbashiru.ba25@gmail.com"
        />
        <Styledheader title="Documentation" />
        <StyledInfo
          icon={IMAGE.avatar}
          name="CEDRIC OBLITEY COMMEY"
          number="0572316846"
          email="******@gmail.com"
        />
        <StyledInfo
          icon={IMAGE.avatar}
          name="AFREH CHRISTIAN BOATENG"
          number="0550678330"
          email="afrehchristian@gmail.com"
        />
        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <Image
            source={Icons.twitter}
            width={20}
            height={20}
            style={{ marginRight: 10 }}
          />
          <Image
            source={Icons.facebook}
            width={20}
            height={20}
            style={{ marginRight: 10 }}
          />
          <Image source={Icons.whatsApp} width={10} height={10} />
        </View>
      </ScrollView>
    </View>
  );
};
export default ContactScreen;
