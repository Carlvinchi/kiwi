import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
} from "react-native";

import Constants from "expo-constants";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

import BackHeader from "../components/BackHeader";
import { IMAGE } from "../constants/Image";
import DefaultStyle from "../constants/Styles/DefaultStyles";
const { container } = DefaultStyle;

import { Formik } from "formik";
import * as yup from "yup";

const { width } = Dimensions.get("window");

const StyledInput = ({ label, formikProps, formikKey, ...rest }) => {
  const inputStyles = {
    borderColor: "#07485B",
    borderWidth: 1.5,
    padding: 10,
    borderRadius: 10,
    height: 50,
    alignItems: "center",
  };
  if (formikProps.touched[formikKey] && formikProps.errors[formikKey]) {
    inputStyles.borderColor = "red";
  }
  return (
    <View style={{ marginRight: 10, marginLeft: 10 }}>
      <Text style={{ marginBottom: 5, marginTop: 5 }}>{label}</Text>
      <View>
        <TextInput
          style={inputStyles}
          onChangeText={formikProps.handleChange(formikKey)}
          onBlur={formikProps.handleBlur(formikKey)}
          {...rest}
        />
      </View>
      <Text style={{ color: "red" }}>
        {formikProps.touched[formikKey] && formikProps.errors[formikKey]}
      </Text>
    </View>
  );
};

const ProdDetails = ({ label, formikProps, formikKey, ...rest }) => {
  const inputStyles = {
    borderColor: "#07485B",
    borderWidth: 1.5,
    padding: 10,
    borderRadius: 10,
    height: 70,
    alignItems: "center",
  };
  if (formikProps.touched[formikKey] && formikProps.errors[formikKey]) {
    inputStyles.borderColor = "red";
  }
  return (
    <View style={{ marginRight: 10, marginLeft: 10 }}>
      <Text style={{ marginBottom: 5, marginTop: 5 }}>{label}</Text>
      <View>
        <TextInput
          style={inputStyles}
          onChangeText={formikProps.handleChange(formikKey)}
          onBlur={formikProps.handleBlur(formikKey)}
          {...rest}
        />
      </View>
      <Text style={{ color: "red" }}>
        {formikProps.touched[formikKey] && formikProps.errors[formikKey]}
      </Text>
    </View>
  );
};

const validationSchema = yup.object().shape({
  prodName: yup
    .string()
    .label("prodName")
    .required("Product Name can't be empty"),
  prodPrice: yup.string().label("prodPrice").required("Please enter the Price"),
  Contact: yup.string().label("Contact").required(),
  WhatsApp: yup.string().label("WhatsApp").required(),
  prodCat: yup.string().label("prodCat").required("Please select a category"),
  prodDet: yup
    .string()
    .label("prodDet")
    .required("Please describe the product"),
});

export default class ProductScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      img: null,
      imDisplay: null,
    };
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        this.setState({ imDisplay: result });
        this.setState({ img: result });
      }

      //console.log(this.state.img);
    } catch (E) {
      console.log(E);
    }
  };

  render() {
    let { image } = this.state;
    return (
      <Formik
        initialValues={{
          prodName: "",
          prodPrice: "",
          Contact: "",
          WhatsApp: "",
          prodCat: "",
          prodDet: "",
        }}
        onSubmit={(values, actions) => {
          setTimeout(async () => {
            actions.setSubmitting(false);
            let userId = "";
            try {
              userId = (await AsyncStorage.getItem("userId")) || "none";
            } catch (error) {
              // Error retrieving data
              console.log(error.message);
            }

            if (userId == "none") {
              alert("You need to be logged in to add a product");
            } else {
              let apiUrl = 'https://warm-ridge-56934.herokuapp.com/addProduct';

              let uri = this.state.img.uri;

              let uriParts = uri.split(".");
              let fileType = uriParts[uriParts.length - 1];

              let formData = new FormData();
              formData.append("productImage", {
                uri,
                name: `productImage.${fileType}`,
                type: `image/${fileType}`,
              });
              formData.append("category", values.prodCat);
              formData.append("title", values.prodName);
              formData.append("description", values.prodDet);
              formData.append("price", values.prodPrice);
              formData.append("contact", values.Contact);
              formData.append("whatsapp", values.WhatsApp);
              formData.append("userID", userId);
              let options = {
                method: "POST",
                body: formData,
                headers: {
                  Accept: "application/json",
                  "Content-Type": "multipart/form-data",
                },
              };

              fetch(apiUrl, options)
                .then((res) => res.json())
                .then(async (data) => {
                  console.log(data);
                  try {
                    alert("Upload sucessfully");
                    this.props.navigation.navigate("Home");
                  } catch (e) {
                    // saving error
                    alert(e);
                  }
                });
            }
          });
        }}
        validationSchema={validationSchema}
      >
        {(formikProps) => (
          <View style={container}>
            <View style={styles.header}>
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Home")}
                >
                  <BackHeader />
                </TouchableOpacity>
                <Image
                  resizeMode="contain"
                  source={IMAGE.AddProd}
                  style={{
                    height: 90,
                    width: width / 4,
                    marginLeft: width / 2 + 35,
                    marginTop: 15,
                  }}
                />
              </View>
              <Text
                style={{ fontSize: 25, color: "white", fontWeight: "bold" }}
              >
                Add Product Screen
              </Text>
            </View>

            <ScrollView>
              <View
                style={{
                  marginLeft: 5,
                  marginRight: 5,
                  backgroundColor: "white",
                }}
              >
                <View style={styles.Card}>
                  <StyledInput
                    label="Product Name:"
                    formikProps={formikProps}
                    formikKey="prodName"
                    placeholder="king"
                    onSubmitEditing={() => Formik.setFocus("Product Price:")}
                  />
                  <StyledInput
                    label="Product Price:"
                    formikProps={formikProps}
                    formikKey="prodPrice"
                  />
                  <StyledInput
                    label="Contact:"
                    formikProps={formikProps}
                    formikKey="Contact"
                  />
                  <StyledInput
                    label="WhatsApp number:"
                    formikProps={formikProps}
                    formikKey="WhatsApp"
                  />
                  <StyledInput
                    label="Product Category"
                    formikProps={formikProps}
                    formikKey="prodCat"
                    placeholder="Cloth"
                  />
                  <ProdDetails
                    label="Product Details"
                    formikProps={formikProps}
                    formikKey="prodDet"
                    multiline={true}
                  />
                </View>
                <View style={styles.Card}>
                  <View>
                    <Text
                      style={{
                        marginBottom: 10,
                        marginLeft: 10,
                        marginTop: 5,
                      }}
                    >
                      Product Image:
                    </Text>

                    <View style={styles.containerImg}>
                      <View style={styles.imageContainer}>
                        {image && (
                          <Image
                            source={{ uri: image }}
                            style={{ width: "100%", height: "100%" }}
                            formikProps={formikProps}
                            formikKey="prodImag"
                          />
                        )}
                      </View>

                      <TouchableOpacity
                        style={styles.buttonImg}
                        onPress={this._pickImage}
                      >
                        <Text
                          style={{
                            color: "#07485B",
                            fontSize: 16,
                            fontWeight: "bold",
                          }}
                        >
                          Choose Image
                        </Text>
                      </TouchableOpacity>
                    </View>

                    <View
                      style={{
                        alignItems: "center",
                        marginTop: 10,
                        marginBottom: 10,
                      }}
                    >
                      <TouchableOpacity
                        style={styles.button}
                        onPress={formikProps.handleSubmit}
                      >
                        <Text style={{ fontSize: 20, color: "white" }}>
                          Upload Product
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        )}
      </Formik>
    );
  }
}

const styles = StyleSheet.create({
  Card: {
    borderRadius: 10,
    backgroundColor: "white",
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 25,
    elevation: 8,
  },
  button: {
    width: "40%",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: "#07485B",
    shadowColor: "black",
    elevation: 5,
  },
  header: {
    paddingHorizontal: 10,
    paddingBottom: 5,
  },
  containerImg: {
    width: "100%",
    alignItems: "center",
  },
  imageContainer: {
    borderWidth: 1,
    borderColor: "black",
    width: "80%",
    height: 150,
  },
  buttonImg: {
    marginTop: 10,
    marginBottom: 5,
    width: "35%",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: "white",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 5,
    shadowRadius: 5,
    elevation: 5,
  },
});
