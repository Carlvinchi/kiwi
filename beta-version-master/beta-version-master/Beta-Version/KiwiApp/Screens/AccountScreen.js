import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import { IMAGE } from "../constants/Image";
import * as Animatable from "react-native-animatable";
import { useTheme } from "react-native-paper";

const AccountScreen = ({ navigation }) => {
  const { colors } = useTheme();
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#07485B" barStyle="light-content" />
      <View style={styles.header}>
        <Animatable.Image
          animation="bounceIn"
          duration={2000}
          resizeMode="contain"
          source={IMAGE.Account}
          style={styles.logo}
        />
      </View>
      <Animatable.View
        style={[styles.footer, { backgroundColor: colors.background }]}
        animation="fadeInUpBig"
      >
        <Text style={[styles.title, { color: colors.text }]}>
          Register with us to get the best of our Services
        </Text>
        <Text style={{ marginBottom: 40, color: "grey" }}>
          Sign in with your Account/ Sign up for free
        </Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate("SignIn")}
        >
          <Text style={styles.btnTxt}>Sign In</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("SignUp")}
          style={[styles.button, { borderWidth: 1.5, borderColor: "#17a3dd" }]}
        >
          <Text style={styles.btnTxt}>Sign up</Text>
        </TouchableOpacity>
      </Animatable.View>
    </View>
  );
};

const { height } = Dimensions.get("screen");
const logo_height = height * 0.28;

export default AccountScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07485B",
  },
  header: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  footer: {
    flex: 1,
    backgroundColor: "white",
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingHorizontal: 30,
    paddingVertical: 50,
    elevation:10,
    marginLeft:5,
    marginRight:5
  },
  logo: {
    width: logo_height,
    height: logo_height,
  },
  title: {
    fontWeight: "bold",
    fontSize: 24,
    color: "#17a3dd",
  },
  btnTxt: {
    fontSize: 20,
    color: "#07485B",
    fontWeight: "bold",
  },
  button: {
    backgroundColor: "white",
    height: 45,
    marginBottom: 20,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 5,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    shadowRadius: 5,
    elevation: 5,
  },
});
