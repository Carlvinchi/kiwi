import React from "react";
import {
  ActivityIndicator,
  Text,
  View,
  CheckBox,
  TextInput,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
} from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import DefaultStyle from "../constants/Styles/DefaultStyles";
import { useTheme } from "react-native-paper";

const {
  container,
  header,
  title,
  footer,
  Submit,
  Forgot,
  SubmitTxt,
} = DefaultStyle;

const StyledInput = ({ label, formikProps, formikKey, Cstyle, ...rest }) => {
  const inputStyles = {
    borderWidth: 2,
    borderColor: "#17a3dd",
    borderRadius: 10,
    height: 50,
    alignItems: "center",
    flexDirection: "row",
  };
  if (formikProps.touched[formikKey] && formikProps.errors[formikKey]) {
    inputStyles.borderColor = "red";
  }
  return (
    <View>
      <Text style={[{ marginBottom: 3 }, { color: Cstyle }]}>{label}</Text>
      <View style={inputStyles}>
        <TextInput
          style={{ marginLeft: 10, flex: 1 }}
          onChangeText={formikProps.handleChange(formikKey)}
          onBlur={formikProps.handleBlur(formikKey)}
          {...rest}
        />
      </View>
      <Text style={{ color: "red" }}>
        {formikProps.touched[formikKey] && formikProps.errors[formikKey]}
      </Text>
    </View>
  );
};

const StyledCheckbox = ({
  navigation,
  formikKey,
  formikProps,
  label,
  CIcon,
  ...rest
}) => (
  <View style={{ alignItems: "center" }}>
    <View style={{ flexDirection: "row" }}>
      <CheckBox
        value={formikProps.values[formikKey]}
        onValueChange={(value) => {
          formikProps.setFieldValue(formikKey, value);
        }}
        {...rest}
      />
      <Text style={{ color: "grey", marginTop: 5 }}>I have accepted the </Text>
      <TouchableOpacity onPress={() => navigation.navigate("Terms")}>
        <Text
          style={{
            marginTop: 5,
            fontWeight: "bold",
            color: "#00A79B",
            textDecorationLine: "underline",
          }}
        >
          {label}
        </Text>
      </TouchableOpacity>
    </View>
    <Text style={{ color: "red" }}>
      {formikProps.touched[formikKey] && formikProps.errors[formikKey]}
    </Text>
  </View>
);

const phoneRegExp = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/;
const validationSchema = yup.object().shape({
  email: yup.string().label("Email").email("Invalid Email").required(),
  password: yup
    .string()
    .label("Password")
    .required()
    .min(6, "Please enter a minimum of 6 characters"),
  Fullname: yup.string().label("Fullname").required().min(2, "Too Short!"),
  Username: yup.string().label("Username").required().min(2, "Too Short!"),
  University: yup.string().label("University").required(),
  Contact: yup
    .string()
    .required()
    .matches(phoneRegExp, "Phone number is invalid")
    .min(10, "Number not complete")
    .max(13),
  Location: yup.string().label("Location").required(),
  TnC: yup
    .boolean()
    .label("Terms & Conditions")
    .required()
    .oneOf([true], "You must accept the terms and conditions"),
});

const SignUp = ({ navigation }) => {
  const { colors } = useTheme();
  return (
    <Formik
      initialValues={{
        Fullname: "",
        Username: "",
        University: "",
        email: "",
        Contact: "",
        Location: "",
        password: "",
        TnC: false,
      }}
      onSubmit={(values, actions) => {
        setTimeout(() => {
          actions.setSubmitting(false);

          fetch("https://warm-ridge-56934.herokuapp.com/signup", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              fullName: values.Fullname,
              email: values.email,
              mobileNumber: values.Contact,
              university: values.University,
              address_1: values.Location,
              userName: values.Username,
              profileImage: "",
              password: values.password,
              resetPasswordToken: "",
              resetPasswordExpires: Date.now(),
            }),
          })
            .then((res) => res.json())
            .then(async (data) => {
              console.log(data);
              if (data.error) {
                alert(data.error);
              } else if (data.err) {
                alert("Username already exists");
              }

              await AsyncStorage.setItem("token", data.token);
              console.log("Your registred");
              navigation.navigate("SignIn");
            });
        }, 1000);
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <View style={container}>
          <View style={header}>
            <Text style={title}>Register Now!</Text>
          </View>
          <View style={[footer, { backgroundColor: colors.background }]}>
            <ScrollView>
              <StyledInput
                Cstyle={colors.text}
                label="Fullname"
                formikProps={formikProps}
                formikKey="Fullname"
              />
              <StyledInput
                Cstyle={colors.text}
                label="Username"
                formikProps={formikProps}
                formikKey="Username"
              />
              <StyledInput
                Cstyle={colors.text}
                label="University"
                formikProps={formikProps}
                formikKey="University"
              />
              <StyledInput
                Cstyle={colors.text}
                label="Email"
                formikProps={formikProps}
                formikKey="email"
                placeholder="kiwiKodex@gmail.com"
              />
              <StyledInput
                Cstyle={colors.text}
                label="Contact"
                formikProps={formikProps}
                formikKey="Contact"
                placeholder="contact"
                keyboardType="numeric"
              />
              <StyledInput
                Cstyle={colors.text}
                label="Location"
                formikProps={formikProps}
                formikKey="Location"
                placeholder="Kotei"
              />
              <StyledInput
                Cstyle={colors.text}
                label="Password"
                formikProps={formikProps}
                formikKey="password"
                placeholder="password"
                secureTextEntry
              />
              <StyledCheckbox
                navigation={navigation}
                label="Terms & Conditions"
                formikProps={formikProps}
                formikKey="TnC"
              />
              <View style={Forgot}>
                <Text style={{ color: "grey" }}>Already have an account? </Text>
                <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
                  <Text
                    style={
                      ({ color: "black", fontWeight: "bold" },
                      { color: colors.text })
                    }
                  >
                    Sign In
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
            {formikProps.isSubmitting ? (
              <ActivityIndicator />
            ) : (
              <TouchableOpacity
                style={[{ borderColor: "#17a3dd", borderWidth: 2 }, Submit]}
                onPress={formikProps.handleSubmit}
              >
                <Text style={SubmitTxt}>Sign Up</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      )}
    </Formik>
  );
};
export default SignUp;
