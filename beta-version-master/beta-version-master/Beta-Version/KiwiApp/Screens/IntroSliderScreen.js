import React, { Component } from "react";
import { StyleSheet, View, Text, Image, Platform } from "react-native";
import IntroSlider from "react-native-app-intro-slider";
import { Introdata } from "../components/Data";

const renderItem = ({ item }) => {
  return (
    <View style={styles.MainContainer}>
      <Image resizeMode="cover" source={item.image} style={styles.image} />
      <Text style={styles.title}>{item.title}</Text>
      <Text style={styles.text}>{item.text}</Text>
    </View>
  );
};

export default class IntroSlide extends Component {
  on_Done_all_slides = () => {
    this.props.navigation.navigate("HomeStack");
  };

  on_Skip_slides = () => {
    this.props.navigation.navigate("HomeStack");
  };

  render() {
    return (
      <View
        style={{ flex: 1, justifyContent: "center", backgroundColor: "white" }}
      >
        <IntroSlider
          slides={Introdata}
          renderItem={renderItem}
          onDone={this.on_Done_all_slides}
          buttonTextStyle={styles.btnTxt}
          dotStyle={styles.dot}
          activeDotStyle={styles.activedot}
          showSkipButton={true}
          onSkip={this.on_Skip_slides}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: Platform.OS === "ios" ? 20 : 0,
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },
  title: {
    fontSize: 30,
    color: "#07485B",
    fontWeight: "bold",
    textAlign: "center",
    marginTop: 20,
  },
  text: {
    color: "black",
    fontSize: 16,
  },
  image: {
    width: 300,
    height: 300,
    resizeMode: "contain",
  },
  btnTxt: {
    color: "#07485B",
  },
  dot: {
    backgroundColor: "grey",
  },
  activedot: {
    backgroundColor: "#07485B",
  },
});
