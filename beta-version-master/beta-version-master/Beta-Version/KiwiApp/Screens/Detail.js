import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  Button,
  Dimensions,
  Linking,
  Platform
} from "react-native";
const { width, height } = Dimensions.get("window");
import { Icons } from "../constants/Image";

export default class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.route.params.productId,
      productPrice: this.props.route.params.productPrice,
      productImage: this.props.route.params.productImage,
      productTitle: this.props.route.params.productTitle,
      productDescription: this.props.route.params.productDescription,
      WhatsApp: this.props.route.params.WhatsAppnumber,
      Contact: this.props.route.params.Contact,
      msg: "I want to find out about the product " + this.props.route.params.productTitle + ", " + this.props.route.params.productPrice
    };
  }
  componentDidMount() {
    //Here is the Trick
    const { navigation } = this.props;
    //Adding an event listner om focus
    //So whenever the screen will have focus it will set the state to zero
    this._unsubscribe = navigation.addListener("focus", () => {
      // do something

      this.setState({
        id: this.props.route.params.productId,
        productPrice: this.props.route.params.productPrice,
        productImage: this.props.route.params.productImage,
        productTitle: this.props.route.params.productTitle,
        productDescription: this.props.route.params.productDescription,
        Contact: this.props.route.params.Contact,
        WhatsApp: this.props.route.params.WhatsApp,
        msg: "I want to find out about the product " + this.props.route.params.productTitle + ", " + this.props.route.params.productPrice
            });
    });
  }

  componentWillUnmount() {
    // Remove the event listener before removing the screen from the stack

    this._unsubscribe();
  }

  makeCall = () => {

    let phoneNumber = '';

    if (Platform.OS === 'android') {

      phoneNumber = `tel:${this.state.Contact}`;

    } else {
      phoneNumber = `telprompt:${this.state.Contact}`;
    }

    Linking.openURL(phoneNumber);
  };

  sendOnWhatsApp=() => {
    let msg = this.state.msg;
    let mobile = this.state.WhatsApp;
    let contact = mobile.substring(1);
    if(mobile){
      if(msg){
        let url = 'whatsapp://send?text=' + this.state.msg + '&phone=233' + contact;
        Linking.openURL(url).then((data) => {
          console.log('WhatsApp Opened');
        }).catch(() => {
          alert('Make sure Whatsapp installed on your device');
        });
      }else{
        alert('Please insert message to send');
      }
    }else{
      alert('Please insert mobile no');
    }
  }
  renderProducts() {
    return (
      <View style={styles.container} key={this.state.id}>
        <View
          style={{
            backgroundColor: "white",
            borderBottomLeftRadius: 20,
            height: 45,
            width: 60,
            marginTop: 5,
            marginLeft: 10,
            shadowColor: "black",
            elevation: 10,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Home")}
          >
            <Text style={{ margin: 5, fontSize: 18 }}>Back</Text>
          </TouchableOpacity>
        </View>
        <View style={{ marginBottom: 50, alignItems: "center" }}>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>
            Product Details Screen
          </Text>
        </View>
        <View
          style={{
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
          }}
        >
          <Text style={{ fontSize: 24 }}>{this.state.productTitle}</Text>
          <Image
            style={styles.image}
            source={{ uri: this.state.productImage }}
          />
          <Text style={[styles.textStyle, { marginTop: 25 }]}>
            {this.state.productDescription}
          </Text>
          <View
            style={{
              backgroundColor: "white",
              width: 150,
              elevation: 5,
              height: 40,
              borderRadius: 10,
              justifyContent: "center",
              alignItems: "center",
              marginTop: 25,
            }}
          >
            <Text style={{ fontSize: 20 }}>
              {"Ghs " + this.state.productPrice}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: "row" }}>
          <View
            style={{
              backgroundColor: "white",
              borderBottomLeftRadius: 10,
              borderTopRightRadius: 10,
              height: 45,
              width: 70,
              marginStart: width / 30,
              alignItems: "center",
              justifyContent: "center",
              marginTop: 50,
              elevation: 10,
            }}
          >
            <TouchableOpacity onPress={this.makeCall}>
              <Image source={Icons.Phone} style={{ margin: 10 }} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              backgroundColor: "#1BD741",
              borderBottomLeftRadius: 10,
              borderTopRightRadius: 10,
              height: 45,
              marginStart: width / 1.7,
              width: 70,
              marginTop: 50,
              alignItems: "center",
              justifyContent: "center",
              elevation: 10,
            }}
          >
            <TouchableOpacity onPress={this.sendOnWhatsApp}>
              <Image source={Icons.whatsApp} style={{ margin: 10 }} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return <View style={styles.container}>{this.renderProducts()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  image: {
    width: width - 20,
    height: height / 3,
    borderRadius: 15,
  },
  textStyle: {
    fontSize: 18,
  },
  cardView: {
    flex: 1,
    width: width - 20,
    height: height / 3,
    backgroundColor: "white",
    margin: 10,
    borderRadius: 15,
    shadowColor: "black",
    elevation: 5,
  },
});
