import React from "react";
import {
  TextInput,
  ActivityIndicator,
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import * as Animatable from "react-native-animatable";
import { Feather } from "@expo/vector-icons";
import { IMAGE } from "../constants/Image";

const StyledInput = ({ label, formikProps, IconName, formikKey, ...rest }) => {
  const inputStyles = {
    borderWidth: 1.5,
    borderColor: "blue",
    borderRadius: 10,
    height: 50,
    alignItems: "center",
    flexDirection: "row",
  };
  if (formikProps.touched[formikKey] && formikProps.errors[formikKey]) {
    inputStyles.borderColor = "red";
  }
  return (
    <View>
      <Text style={{ marginBottom: 3 }}>{label}</Text>
      <View style={inputStyles}>
        <View style={{ marginRight: 10, marginLeft: 10 }}>
          <Feather name={IconName} color="black" size={25} />
        </View>
        <TextInput
          style={{ fontSize: 18, flex: 1, paddingRight: 10 }}
          onChangeText={formikProps.handleChange(formikKey)}
          onBlur={formikProps.handleBlur(formikKey)}
          {...rest}
        />
      </View>
      <Text style={{ color: "red" }}>
        {formikProps.touched[formikKey] && formikProps.errors[formikKey]}
      </Text>
    </View>
  );
};

const validationSchema = yup.object().shape({
  email: yup.string().label("Email").email().required(),
});

const ForgotScreen = ({ navigation }) => {
  return (
    <Formik
      initialValues={{ email: "" }}
      onSubmit={(values, actions) => {
        alert(JSON.stringify(values));
        setTimeout(() => {
          actions.setSubmitting(false);


          fetch("https://warm-ridge-56934.herokuapp.com/forgotpassword", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: values.email,

            }),
          })
            .then((res) => res.json())
            .then(async (data) => {
              console.log(data);
              if (data.error) {
                alert(data.error);
              }

              await AsyncStorage.setItem("token", data.token);
              await AsyncStorage.setItem("userId", data.userID);
              console.log("You are Logged in");
              navigation.navigate("Home");
            });

        }, 1000);
      }}
      validationSchema={validationSchema}
    >
      {(formikProps) => (
        <View style={styles.container}>
          <View style={styles.header}>
            <Animatable.Image
              animation="bounceIn"
              duration={2000}
              resizeMode="contain"
              style={styles.logo}
              source={IMAGE.Forgot}
            />
            <Text style={styles.title}>Forgot Password?</Text>
            <Text style={styles.txt}>
              Don't worry we just need your{"\n"} registered email address and
              its done!
            </Text>
          </View>
          <Animatable.View style={styles.footer} animation="fadeInUpBig">
            <StyledInput
              label="Email"
              IconName="mail"
              formikProps={formikProps}
              formikKey="email"
              placeholder="kiwiKodex@gmail.com"
            />
            {formikProps.isSubmitting ? (
              <ActivityIndicator />
            ) : (
              <TouchableOpacity
                style={styles.Submit}
                onPress={formikProps.handleSubmit}
              >
                <Text style={{ fontSize: 25, color: "#07485B" }}>
                  Reset Password
                </Text>
              </TouchableOpacity>
            )}
          </Animatable.View>
        </View>
      )}
    </Formik>
  );
};

const { height, width } = Dimensions.get("screen");
const logo_height = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07485B",
  },
  header: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    justifyContent: "flex-start",
    fontWeight: "bold",
    fontSize: 24,
    color: "white",
  },
  txt: {
    color: "red",
    textAlign: "center",
  },
  footer: {
    flex: 1,
    backgroundColor: "white",
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingHorizontal: 30,
    paddingVertical: 50,
    marginLeft: 5,
    marginRight: 5,
    elevation: 15,
  },
  Submit: {
    backgroundColor: "white",
    height: 45,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 5,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
  },
  Forgot: {
    marginTop: 20,
    marginBottom: 20,
    flexDirection: "row",
    justifyContent: "center",
  },
  logo: {
    width: logo_height,
    height: logo_height,
  },
});
export default ForgotScreen;
