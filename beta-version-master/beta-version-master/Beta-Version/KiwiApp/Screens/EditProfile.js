import React, { useEffect, useState, Component } from "react";
import {
  TextInput,
  ActivityIndicator,
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
} from "react-native";
import { IMAGE } from "../constants/Image";
import { Feather } from "@expo/vector-icons";
import { Formik } from "formik";
import * as yup from "yup";
import { Avatar } from "react-native-paper";

import Constants from "expo-constants";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import ImagePickerContainer from "../components/ImagePickerContainer";

export default class EditProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      img: null,
      imDisplay: null,
    };
  }

  _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
      if (!result.cancelled) {
        this.setState({ image: result.uri });
        this.setState({ imDisplay: result });
        this.setState({ img: result });
      }

      //console.log(this.state.img);
    } catch (E) {
      console.log(E);
    }
  };
  render() {
    let { image } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <ImageBackground style={{ flex: 1 }} source={IMAGE.background}>
            <View style={styles.containerImg}>
              <View style={styles.imageContainer}>
                {image && <Avatar.Image source={{ uri: image }} size={100} />}
              </View>
            </View>
            <View style={{ alignItems: "flex-end", marginRight: 10 }}>
              <TouchableOpacity style={styles.icon} onPress={this._pickImage}>
                <Feather name="camera" color="white" size={25} />
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.footer}>
          <Text>Hello</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#07485B",
  },
  header: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
  },
  Title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "black",
    marginBottom: 3,
  },
  footer: {
    flex: 2,
    backgroundColor: "white",
    justifyContent: "center",
    paddingHorizontal: 30,
    paddingVertical: 50,
  },
  Submit: {
    backgroundColor: "white",
    height: 45,
    width: 70,
    shadowColor: "black",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
  },
  image: {
    flex: 1,
    resizeMode: "contain",
    justifyContent: "center",
  },
  icon: {
    backgroundColor: "#07485B",
    borderRadius: 50,
    height: 35,
    width: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  imageContainer: {
    borderWidth:1,
    borderColor: "#FFF",
    width: "40%",
    height: "80%",
    borderRadius:100
  },
  text: {
    fontSize: 16,
    color: "grey",
    marginBottom: 10,
  },
  containerImg: {
    marginTop:10,
    alignItems: "center",
  },
});
