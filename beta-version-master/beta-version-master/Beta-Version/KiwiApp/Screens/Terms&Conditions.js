import React from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import BackHeader from "../components/BackHeader";
import DefaultStyle from "../constants/Styles/DefaultStyles";
const { container, TermsTitle, Termstxt } = DefaultStyle;
import { useTheme } from "react-native-paper";

const TextInputStyle = ({ Label, CStyle }) => {
  return (
    <View>
      <Text style={[TermsTitle, { color: CStyle }]}>{Label}</Text>
    </View>
  );
};

const StyledContent = ({ Label, CStyle }) => {
  return (
    <View>
      <Text style={[Termstxt, { color: CStyle }]}>{Label}</Text>
    </View>
  );
};

const Terms = ({ navigation }) => {
  const { colors } = useTheme();
  return (
    <View style={container}>
      <View style={{ marginLeft: 10 }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <BackHeader />
        </TouchableOpacity>
      </View>
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Text style={{ fontSize: 20, marginTop: 30, color: "white" }}>
          Terms of Service
        </Text>
        <View
          style={[
            {
              margin: 10,
              backgroundColor: "#FFF",
              borderRadius: 20,
              marginBottom: 180,
            },
            { backgroundColor: colors.background },
          ]}
        >
          <ScrollView style={{ margin: 10 }}>
            <TextInputStyle Label={"INTRODUCTION"} CStyle={colors.text} />
            <StyledContent
              Label={
                "Welcome to kiwi mart site. These terms and conditions apply to the Site, and all of its divisions, subsidiaries, and affiliate operated Internet sites which reference these Terms and conditions."
              }
              CStyle={colors.text}
            />

            <StyledContent
              Label={
                " By accessing the Site, you confirm your understanding of the Terms and Conditions. If otherwise, you shall not use this app. The app reserves the right, to change, modify, add, or remove portions of these Terms and Conditions of use at any time. Changes will be effective when posted on the Site with no other notice provided. Please check these Terms and Conditions of use regularly for updates. Your continued use of the Site following the posting of changes to these Terms and Conditions of use constitutes your acceptance of those changes."
              }
              CStyle={colors.text}
            />
            <TextInputStyle Label={"USE OF THE SITE"} CStyle={colors.text} />
            <StyledContent
              Label={
                "You are either responsible or using it under the guidance of a parent or a legal guardian. The right to use the site is non-transferable and revocable under the terms and conditions described, for the purpose of buying and selling of on the site. Any third party usage will need permission from us. Any breach of this result in revocation of license without notice to you. Products on this site are solely for informational purposes and any information provided on the product is totally the work of the vendor and not us. As such content may not reflect our ideas. Certain features on the app requires registrations. Should you choose to register, you agree to provide accurate information and update them as and when they may change. In such situation, you are responsible for keeping your account details and password safe and secure as you are responsible for all activities on the account."
              }
              CStyle={colors.text}
            />
            <TextInputStyle Label={"USER SUBMISSIONS"} CStyle={colors.text} />
            <StyledContent
              Label={
                "Anything that you submit to the Site and/or provide to us, including but not limited to, questions, reviews, comments, and suggestions will become our sole and exclusive property and shall not be returned to you. You shall not make any submissions under false usernames or emails as it can be misleading to third parties."
              }
              CStyle={colors.text}
            />
            <TextInputStyle
              Label={"ORDER ACCEPTANCE AND PRICING"}
              CStyle={colors.text}
            />
            <StyledContent
              Label={
                "The order acceptance, item information and pricing is solely the responsibility of the vendor. But we have the right to interrogate any misleading pricing or information on a particular product."
              }
              CStyle={colors.text}
            />
            <TextInputStyle Label={"TERMINATION"} CStyle={colors.text} />
            <StyledContent
              Label={
                "In addition to any other legal or equitable remedies, we may, without prior notice to you, immediately terminate the Terms and Conditions or revoke any or all of your rights granted under the Terms and Conditions. Upon any termination of this Agreement, you shall immediately cease all access to and use of the Site and we shall, in addition to any other legal or equitable remedies,immediately revoke all password(s) and account identification issued to you and deny your access to and use of this Site in whole or in part. Any termination of this agreement shall not affect the respective rights and obligations (including without limitation, payment obligations) of the parties arising before the date of termination. You furthermore agree that the Site shall not be liable to you or to any other person as a result of any such suspension or termination. If you are dissatisfied with the Site or with any terms, conditions, rules, policies, guidelines, or practices of Kiwi Mart Services in operating the Site, your sole and exclusive remedy is to discontinue using the Site."
              }
              CStyle={colors.text}
            />
          </ScrollView>
        </View>
      </View>
    </View>
  );
};
export default Terms;
