import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image,Dimensions } from "react-native";
import { Avatar } from "react-native-paper";
import { IMAGE, Profile } from "../constants/Image";
import { MaterialIcons } from "@expo/vector-icons";

const { height, width } = Dimensions.get("screen");

const ProfileScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={{ flexDirection: "row" }}>
          <Image source={Profile.styles2} style={{ width: 50, height: 50,margin:10 }} />
          <Image source={Profile.styles1} style={{ width: 40, height: 40,marginLeft:width-140 }} />
        </View>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Text style={{ fontSize: 20, color: "#000" }}>Profile Screen</Text>
          <Avatar.Image
            source={IMAGE.avatar}
            size={100}
            style={{ marginTop:10 }}
          />
        </View>
      </View>
      <View style={styles.footer}>
        <View style={{ marginLeft: 20 }}>
          <Text style={styles.Title}>Username</Text>
          <Text style={styles.text}>kiwikodex</Text>
          <Text style={styles.Title}>Email</Text>
          <Text style={styles.text}>kiwikodex@gmail.com</Text>
          <Text style={styles.Title}>Contact</Text>
          <Text style={styles.text}>+233557462789</Text>
          <Text style={styles.Title}>Campus</Text>
          <Text style={styles.text}>KNUST</Text>
          <Text style={styles.Title}>Address</Text>
          <Text style={styles.text}>Kotei</Text>
        </View>
        <View style={{ alignItems: "flex-end", marginRight: 20 }}>
          <TouchableOpacity
            style={styles.Infoicon}
            onPress={() => navigation.navigate("EditProfile")}
          >
            <MaterialIcons name="edit" color="white" size={25} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignContent: "center",
    flex: 1,
    backgroundColor: "#FFF",
  },
  header: {
    flex: 1,
    backgroundColor: "white",
    elevation: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: 10,
    marginRight: 10,
  },
  footer: {
    flex: 2,
    backgroundColor: "white",
    justifyContent: "center",
  },
  icon: {
    marginTop: 90,
    backgroundColor: "white",
    borderRadius: 50,
    height: 35,
    width: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  Infoicon: {
    marginTop: 90,
    backgroundColor: "red",
    borderRadius: 50,
    height: 35,
    width: 35,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "black",
    elevation: 10,
  },
  Title: {
    fontSize: 20,
    fontWeight: "bold",
    color: "black",
  },
  text: {
    fontSize: 16,
    color: "grey",
    marginBottom: 10,
  },
});
