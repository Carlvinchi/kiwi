import React from "react";
import { SafeAreaView, StyleSheet, View } from "react-native";
import styled from "styled-components";

const CategoryScreen = () => {
  return (
    <Container>
      <SafeAreaView>
        <ClothesIcon source={require("../assets/Images/clothes.png")} />
        <PhoneIcon source={require("../assets/Images/iphone.png")} />
        <ShoeIcon source={require("../assets/Images/shoe.png")} />
        <LapIcon source={require("../assets/Images/laptop.png")} />
        <ElectroIcon source={require("../assets/Images/circuit.png")} />
        <OthersIcon source={require("../assets/Images/time.png")} />
        <GameIcon source={require("../assets/Images/game.png")} />
        <AccessIcon source={require("../assets/Images/headset1.png")} />
        <Laptop>Laptops</Laptop>
        <ElectronicsName>Electronics</ElectronicsName>
        <ClothesName>Clothing</ClothesName>
        <OthersName>Others</OthersName>
        <PhoneName>Phones</PhoneName>
        <ShoeName>Shoes</ShoeName>
        <GameName>Games</GameName>
        <AccesoriesName>Accessories</AccesoriesName>
        <TitleBar>
          <Title>CHOOSE YOUR CATEGORY</Title>
          <View style={styles.container}></View>
        </TitleBar>
      </SafeAreaView>
    </Container>
  );
};

export default CategoryScreen;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

const Laptop = styled.Text`
  font-size: 15px;
  top: 228px;
  left: 193px;

  color: #3c4560;
`;
const ElectronicsName = styled.Text`
  font-size: 15px;
  top: 314px;
  left: 190px;

  color: #3c4560;
`;
const ClothesName = styled.Text`
  font-size: 15px;
  top: 292px;
  left: 65px;

  color: #3c4560;
`;
const OthersName = styled.Text`
  font-size: 15px;
  top: 360px;
  left: 190px;

  color: #3c4560;
`;
const PhoneName = styled.Text`
  font-size: 15px;
  top: 143px;
  left: 65px;

  color: #3c4560;
`;
const AccesoriesName = styled.Text`
  font-size: 15px;
  top: -35px;
  left: 65px;

  color: #3c4560;
`;
const ShoeName = styled.Text`
  font-size: 15px;
  top: 315px;
  left: 65px;

  color: #3c4560;
`;
const GameName = styled.Text`
  font-size: 15px;
  top: -13px;
  left: 193px;

  color: #3c4560;
`;
const PhoneIcon = styled.Image`
  width: 60px;
  height: 60px;
  background: white;
  margin-left: 20px;
  border-radius: 10px;
  position: absolute;
  top: 158px;
  left: 40px;
`;
const ClothesIcon = styled.Image`
  width: 65px;
  height: 55px;
  background: white;
  border-radius: 20px;
  margin-left: 20px;
  position: absolute;
  top: 270px;
  left: 40px;
`;
const ShoeIcon = styled.Image`
  width: 65px;
  height: 30px;
  background: white;
  border-radius: 5px;
  margin-left: 20px;
  position: absolute;
  top: 380px;
  left: 40px;
`;

const LapIcon = styled.Image`
  width: 60px;
  height: 60px;
  background: white;
  margin-left: 20px;
  border-radius: 10px;
  position: absolute;
  top: 158px;
  left: 170px;
`;
const ElectroIcon = styled.Image`
  width: 55px;
  height: 55px;
  background: white;
  border-radius: 20px;
  margin-left: 20px;
  position: absolute;
  top: 270px;
  left: 170px;
`;
const OthersIcon = styled.Image`
  width: 50px;
  height: 50px;
  background: white;
  border-radius: 20px;
  margin-left: 20px;
  position: absolute;
  top: 370px;
  left: 170px;
`;
const GameIcon = styled.Image`
  width: 55px;
  height: 55px;
  background: white;
  border-radius: 20px;
  margin-left: 20px;
  position: absolute;
  top: 55px;
  left: 170px;
`;
const AccessIcon = styled.Image`
  width: 50px;
  height: 50px;
  background: white;
  border-radius: 20px;
  margin-left: 20px;
  position: absolute;
  top: 55px;
  left: 50px;
`;

const Container = styled.View`
  flex: 1;
  background-color: white;
`;

const Title = styled.Text`
  font-size: 12px;
  color: teal;
  top: -210px;
  left: 10px;
`;
const TitleBar = styled.View`
  width: 100%;
  margin-top: 50px;
  padding-left: 70px;
`;
