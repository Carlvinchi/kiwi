import React, { useState } from "react";
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Modal,
} from "react-native";
import { useTheme, TouchableRipple, Switch } from "react-native-paper";
import { Card } from "native-base";
import { Icons } from "../constants/Image";
import Contact from "./ContactScreen";
import DefaultStyle from "../constants/Styles/DefaultStyles";
import { AuthContext } from "../components/context";

const { height, width } = Dimensions.get("screen");

const { preference } = DefaultStyle;

const StyledList = ({ label, LeftIconName }) => {
  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        margin: 20,
      }}
    >
      <View style={{ marginRight: 10 }}>
        <Image style={{ width: 20, height: 20 }} source={LeftIconName} />
      </View>
      <Text style={{ alignItems: "flex-end", flex: 1, fontSize: 18 }}>
        {label}
      </Text>
      <View style={{ marginLeft: 50 }}>
        <Image source={require("../assets/Icons/rightArrow.png")} />
      </View>
    </View>
  );
};

const SettingsScreen = ({ navigation }) => {
  const paperTheme = useTheme();
  const { toggleTheme } = React.useContext(AuthContext);
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View>
      <Card style={{ marginBottom: 1 }}>
        <Text style={{ fontWeight: "bold", margin: 10, fontSize: 20 }}>
          Profile
        </Text>
        <TouchableOpacity onPress={() => navigation.navigate("EditProfile")}>
          <StyledList label="Edit Profile" LeftIconName={Icons.profile} />
        </TouchableOpacity>
        <View style={preference}>
          <View style={{ marginRight: 10 }}>
            <Image style={{ width: 20, height: 20 }} source={Icons.brush} />
          </View>
          <Text style={{ alignItems: "flex-end", flex: 1, fontSize: 18 }}>
            Theme
          </Text>
          <TouchableRipple
            onPress={() => {
              toggleTheme();
            }}
          >
            <View pointerEvents="none">
              <Switch value={paperTheme.dark} />
            </View>
          </TouchableRipple>
        </View>
      </Card>
      <Card>
        <Text style={{ fontWeight: "bold", margin: 10, fontSize: 20 }}>
          Advanced
        </Text>
        <TouchableOpacity>
          <StyledList
            label="Enable Notification"
            LeftIconName={Icons.notification}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <StyledList label="App info" LeftIconName={Icons.Info} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("FAQ")}>
          <StyledList label="FAQ" LeftIconName={Icons.Help} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setModalVisible(true);
          }}
        >
          <StyledList label="Contact Us" LeftIconName={Icons.Phone} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Terms")}>
          <StyledList label="Terms and Conditions" LeftIconName={Icons.Terms} />
        </TouchableOpacity>
        <Modal
          transparent={true}
          visible={modalVisible}
          animationType="slide"
          onRequestClose={() => setModalVisible(!modalVisible)}
        >
          <View style={{ flex: 1, backgroundColor: "#000000aa" }}>
            <View
              style={{
                backgroundColor: "#ffffff",
                marginTop: 300,
                flex: 1,
              }}
            >
              <Contact />
            </View>
          </View>
        </Modal>
      </Card>
    </View>
  );
};
export default SettingsScreen;
