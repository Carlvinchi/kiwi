const IMAGE = {
  ICON_USER_DEFAULT: require("../assets/Images/avatar.png"),
  avatar: require("../assets/Images/avatar.png"),
  SignUpLogo: require("../assets/Images/SignUpLogo.png"),
  background: require("../assets/Images/introbackground.png"),
  logo: require("../assets/Images/logo.png"),
  Accbackground: require("../assets/Images/Accbackground.png"),
  shoe: require("../assets/Images/shoe.jpeg"),
  watch: require("../assets/Images/watches.jpg"),
  AddProd: require("../assets/Images/addProd.png"),
  Account: require("../assets/Images/Account.png"),
  Forgot: require("../assets/Images/forgot.png"),
};
const Intro = {
  intro1: require("../assets/Images/intro1.png"),
  intro2: require("../assets/Images/intro2.png"),
  intro3: require("../assets/Images/intro3.png"),
  intro4: require("../assets/Images/intro4.png"),
  intro5: require("../assets/Images/intro5.png"),
};
const Sliders = {
  Slider1: require("../assets/Images/Slider1.png"),
  Slider2: require("../assets/Images/Slider2.png"),
  Slider3: require("../assets/Images/Slider3.png"),
  Slider4: require("../assets/Images/Slider4.png"),
};
const Icons = {
  Copyrigth: require("../assets/Icons/Copyright.png"),
  Help: require("../assets/Icons/Help.png"),
  Info: require("../assets/Icons/info.png"),
  notification: require("../assets/Icons/notification.png"),
  Phone: require("../assets/Icons/phone.png"),
  profile: require("../assets/Icons/profile.png"),
  Terms: require("../assets/Icons/Terms.png"),
  brush: require("../assets/Icons/brush.png"),
  twitter: require("../assets/Icons/twitter.png"),
  facebook: require("../assets/Icons/Facebook.png"),
  whatsApp: require("../assets/Icons/whatsApp.png"),
  LinkedIn: require("../assets/Icons/LinkedIn.png"),
};
const Pomade = {
  Pomade1: require("../assets/Images/Pomade1.jpg"),
  Pomade2: require("../assets/Images/Pomade2.jpg"),
  Pomade3: require("../assets/Images/Pomade3.jpg"),
  Pomade4: require("../assets/Images/Pomade4.jpg"),
  Pomade5: require("../assets/Images/Pomade5.jpg"),
};

const Laptop = {
  Laptop1: require("../assets/Images/Laptop1.jpg"),
  Laptop2: require("../assets/Images/Laptop2.jpg"),
  Laptop3: require("../assets/Images/Laptop3.jpg"),
  Laptop4: require("../assets/Images/Laptop4.jpg"),
  Laptop5: require("../assets/Images/Laptop5.jpg"),
};

const banner = {
  banner1: require("../assets/Images/watches.jpg"),
  banner2: require("../assets/Images/shoe.jpeg"),
  banner3: require("../assets/Images/watch.png"),
  banner4: require("../assets/Images/watches2.jpg"),
};

const Profile ={
  styles1: require("../assets/Icons/styles1.png"),
  styles2: require("../assets/Icons/styles2.png")
}
export { IMAGE, banner, Icons, Sliders, Pomade, Laptop, Intro,Profile };
